const Dotenv = require("dotenv-webpack");

module.exports = {
  devServer: {
    proxy: {
      "^/api/(boards|crates|links)": {
        target: "http://shep-api:8000",
        // changeOrigin
      },
      "/api/board-proxy": {
        target: "http://unused-when-router-set",
        router: function (req) {
          const re = /\/api\/board-proxy\/(?<host_and_port>[^/]+)(\/.*)?/;
          const match = req.url.match(re);
          const new_target = "http://" + match.groups.host_and_port;
          // console.log('Routing board proxy', req.method, 'request for', req.url, 'to', new_target)
          return new_target;
        },
        pathRewrite: { "^/api/board-proxy/[^/]+": "" },
      },
    },
  },
  runtimeCompiler: true,
  // NOTE: set alias via `configureWebpack` or `chainWebpack`
  configureWebpack: {
    resolve: {
      alias: {
        "balm-ui-plus": "balm-ui/dist/balm-ui-plus.js",
        "balm-ui-css": "balm-ui/dist/balm-ui.css",
      },
    },
    plugins: [
      new Dotenv({
        expand: true,
        ignoreStub: true,
      }),
    ],
  },
};
