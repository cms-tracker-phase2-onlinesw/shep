import { createRouter, createWebHistory } from "vue-router";
import CommandsPage from "../views/Commands.vue";
import BoardStatusPage from "../views/BoardStatus.vue";
import NotFoundView from "../views/NotFound.vue";
import StateMachinesPage from "../views/StateMachines.vue";
import SystemSummaryPage from "../views/SystemSummary.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: SystemSummaryPage,
  },
  {
    path: "/board-status",
    name: "Board status",
    component: BoardStatusPage,
  },
  {
    path: "/commands",
    name: "Commands",
    component: CommandsPage,
  },
  {
    path: "/state-machines",
    name: "State machines",
    component: StateMachinesPage,
  },
  {
    path: "/:pathMatch(.*)*",
    name: "Page Not Found",
    component: NotFoundView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: routes,
});

export default router;
