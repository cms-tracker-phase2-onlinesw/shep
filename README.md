# Shep application

Shep is an application that supervises a group of ATCA boards developed for the CMS phase-2 upgrades.
It provides both a gateway and GUI for users and higher-level applications to control and monitor HERD
applications (and their plugins) running on ATCA boards. The Shep application of two main components:

 * The frontend: A web-based user interface (UI), implemented as a set of static files that are loaded by users' browsers
 * The backend: An API server, which sends dynamic system-level data (e.g. the list of boards) to the UI.


## Frontend

The frontend is implemented in the [Vue.js](https://v3.vuejs.org/) framework (version 3), using
components from the [BalmUI](https://next-material.balmjs.com) library.


### Usage

This repository's CI pipeline automatically packages the frontend in a docker image, and publishes
this in the GitLab container registry with the following URL:
 * For tags: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep/shep-ui:vX.Y.Z`
 * For commits: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep/shep-ui:BRANCH_NAME_SLUG-SHORTSHA`,
   where:
    * `BRANCH_NAME_SLUG` is the branch name, with `/` and other invalid characters are replaced by `-`
    * `SHORTSHA` is the first 8 characters of the git commit SHA
    * For example: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep/shep-ui:ui-vue-a9c65d68`


### Build instructions

In order to build the UI, you first need to install NPM, and then install the dependencies listed
in the `package*.json` files. The latter can be done as follows:
```
cd ui
npm clean-install
```

During development, you can build and serve the application locally by running the following command
(from the `ui` directory):
```
npm run serve
```
This command also supports hot-reloads, such that the UI displayed in your browser will automatically
update each time you save changes. The UI will typically be served on port 8080 or 8081 (the specific
port chosen is always printed by the command). Requests to `/api/boards` and `/api/board-proxy` will
be redirected by the development server to port 80 (this behaviour is set in `ui/vue.config.js`).

You can build the code (incl. minification) by running:
```
npm run build
```

Finally, you can run the linter as follows:
```
npm run lint
```
